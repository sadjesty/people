package com.intern.people.personcreator;

import com.intern.people.person.Person;

import java.util.Scanner;

public class PersonCreator {
    public static Person createPerson() {
        try(Scanner scanner = new Scanner(System.in)) {
            System.out.print("Enter first name: ");
            String firstName = scanner.nextLine();
            System.out.println("Enter sex (M/F): ");
            String sex = scanner.nextLine();
            System.out.println("Enter last name: ");
            String lastName = scanner.nextLine();
            System.out.println("Enter middle name: ");
            String middleName = scanner.nextLine();
            System.out.println("Enter age: ");
            String age = scanner.nextLine();
            System.out.println("Enter date of birth (dd.mm.yyyy): ");
            String dateOfBirth = scanner.nextLine();

            return new Person(firstName, lastName, middleName, age, sex, dateOfBirth);
        }
    }
}
