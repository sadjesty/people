package com.intern.people.exercise;

import com.intern.people.person.Person;

import java.util.List;

public class finderPattern {
    public boolean ifAtLeastOnePersonWithoutDateOfBirth(List<Person> people) {
        return people.stream().anyMatch((p) ->
                p.getDateOfBirth() == null &&
                        p.getFirstName().isEmpty()
        );
    }
    public boolean ifAtLeastOnePersonIsWomanOlderThanSixty(List<Person> people) {
        return people.stream().anyMatch((p) ->
            p.getSex().equals("F") &&
                    (Integer.parseInt(p.getAge()) > 60)
        );
    }
    public boolean ifAtLeastOnePersonWithNameMikeAndMiddleNameMikhailovich(List<Person> people) {
        return people.stream().anyMatch((p) ->
                p.getFirstName().equals("Mike") &&
                        p.getMiddleName().equals("Mikhailovich")
        );
    }
}